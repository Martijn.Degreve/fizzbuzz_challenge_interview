# Starter project for a recruitment challenge

This repository is here to help you get started quickly to solve the recruitment challenge

## Solving the challenge

You are free to choose any technology you like, if there is no starter project available, feel free to create your own.

## Submitting your solution

To submit your solution, please create a new git repo on your own account and send us back the url.
Please make sure all access rights are set correctly.


## Tips

Please follow all the best practices you know to solve this project in the best way possible.
This includes writing clean code, optimized use of git, possible dependency management, testing...

Please include any steps necessary to execute the program.

# Challenge

## FizzBuzz

Write a program that prints the numbers from 1 to 100. But for multiples of three print `Fizz` instead of the number and for the multiples of five print `Buzz`. For numbers which are multiples of both three and five print `FizzBuzz`.

Please also include any test you deem necessary.

Sample output:

```
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
... etc up to 100
```





